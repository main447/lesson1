public class Task1 {
    public static void main(String[] args) {

        String p1 = Integer.toHexString(230601).toUpperCase();
        System.out.println(p1);

        long p2 = 89229604577L;
        System.out.println(p2);

        int st;
        String p3 = String.valueOf(p2);
        p3 = p3.replaceAll("0", "");
        p3 = p3.substring(p3.length() - 2);
        st = Integer.parseInt(p3);
        p3 = Integer.toOctalString(st);
        System.out.println(p3);

        int st1;
        String p4 = String.valueOf(p2);
        p4 = p4.replaceAll("0", "");
        p4 = p4.substring(p4.length() - 4);
        st1 = Integer.parseInt(p4);
        p4 = Integer.toBinaryString(st1);
        System.out.println(p4);

        int p5 = ((1 - 1) % 26) + 1;
        System.out.println(p5);

        char p6 = (char) (p5 + 64);
        System.out.println(p6);

    }
}